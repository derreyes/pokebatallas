
package UIPokeBatallas;

import java.util.Scanner;
import pokebatallas.Entrenador;
import pokebatallas.Pokemon;
import pokebatallas.PokeBatallas;


/**
 *
 * @author Grupo: Erreyes, Fernandez, Moreno 
 */
public class Main {
    PokeBatallas batalla= new PokeBatallas();
     static Scanner sc = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
      Main main = new Main();
      main.menu();
    }
    /**
     * Muestra el menu por consola.
     */

    
    public void menu(){
        String opcion="";
        while(!opcion.equals("4")){
              System.out.println("╔              PokeBatallas                 ╗");
              System.out.println("║ 1. Registro de Pokemons                   ║");
              System.out.println("║ 2. Registro de Entrenador                 ║");
              System.out.println("║ 3. Consulta de Pokemons                   ║");                        
              System.out.println("║ 4. Salir                                  ║");        
              System.out.println("╚                                           ╝");
              System.out.print("Ingrese opcion: ");      
              opcion = sc.next();
            switch (opcion){
                case "1":
                    ingresarPokemon();
                    break;
                case "2":
                    ingresarEntrenador();
                    break;
                case "3": 
                    consultarPokemon();
                    break;
                case "4":    
                    System.out.println("Gracias por tu visita!");
                    break;
                default:
                    System.out.println("Opcion No valida!! \n");
            }
        }
        sc.close();
    }
    public void ingresarPokemon(){
        System.out.print("Ingrese el nombre del pokemon: ");
        String nombre=sc.next();
        System.out.print("Ingrese la especie del pokemon: ");
        String especie=sc.next();
        System.out.print("Ingrese el nivel del pokemon(numero): ");
        int nivel=sc.nextInt();
        System.out.print("Ingrese porder de ataque(numero): ");
        int ataque=sc.nextInt();
        System.out.print("Ingrese porder de defensa(numero): ");
        int defensa=sc.nextInt();
        Pokemon pokemon= new Pokemon(nombre,especie,ataque,defensa,nivel);
        batalla.agregarPokemon(pokemon);
        System.out.println("Pokemon registrado exitosamente! \n");
    }
    public void ingresarEntrenador(){
        System.out.print("Ingrese su nombre: ");
        String nombre = sc.next();
        System.out.print("Ingrese el nombre de su pueblo: ");
        String pueblo = sc.next();
        System.out.print("Ingrese su edad: ");
        int edad=sc.nextInt();
        System.out.print("Es lider de gimnasio? s/n: ");
        String esLider= sc.next();
        if(esLider.equals("s")){
           Entrenador entrenador=new Entrenador(nombre,pueblo,edad,true);
           batalla.agregarEntrenador(entrenador);
        } 
        else{
        Entrenador entrenador=new Entrenador(nombre,pueblo,edad,false);
        batalla.agregarEntrenador(entrenador);
        }
        System.out.println("Se ha registrado exitosamente como entrenador \n");
        }
    
    public void consultarPokemon(){
        System.out.print("Ingrese el nombre del pokemon: ");
        String nombre=sc.next();
        if (batalla.verificarPokemon(nombre)==true){
           for (Pokemon p: batalla.obtenerListaPokemon()){
            if (p.getNombre().equals(nombre)){
                System.out.println("Nombre: "+p.getNombre()+"-- Especie: "+p.getEspecie()+"-- Nivel: "+p.getNivel()+"-- Ataque y Defensa: "+p.getAtaque()+"-"+p.getDefensa()+"\n");
                break;
            }         
        }  
        }
        else {
           System.out.println("El Pokemon ingresado no se encuentra resgistrado. \n"); 
        }  
            }
}

