
package pokebatallas;

/**
 *
 * @Sueanny Moreno 
 */
public class Entrenador {
    private String nombre, pueblo;
    private int edad;
    private boolean esLiderGimnasio;

    //Constructor vacio
    public Entrenador(){
    
}
    //constructor copia
    
    public Entrenador(Entrenador pokemon){
         this.nombre=pokemon.nombre;
         this.pueblo=pokemon.pueblo;
         this.edad=pokemon.edad;
         this.esLiderGimnasio=pokemon.esLiderGimnasio;
                
    }
    
    //Constructor por parametros
    public Entrenador(String nombre, String pueblo, int edad, boolean esLiderGimnasio) {
        this.nombre = nombre;
        this.pueblo = pueblo;
        this.edad = edad;
        this.esLiderGimnasio = esLiderGimnasio;
    }
    //obteniendo los getters y setters, dado que los atributos son privados 

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPueblo() {
        return pueblo;
    }

    public void setPueblo(String pueblo) {
        this.pueblo = pueblo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public boolean isEsLiderGimnasio() {
        return esLiderGimnasio;
    }

    public void setEsLiderGimnasio(boolean esLiderGimnasio) {
        this.esLiderGimnasio = esLiderGimnasio;
    }
    
    
}

