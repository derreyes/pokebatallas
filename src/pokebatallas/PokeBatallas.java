/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokebatallas;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HENRY
 */
public class PokeBatallas {
 
//Listas que almacenan entrenadores y pokemons
private List<Entrenador> entrenadores = new ArrayList<>();
private List<Pokemon> pokemons = new ArrayList<>(); 

//Añadir un nuevo entrenador a la lista entrenadores
public void agregarEntrenador(Entrenador entrenador){
    entrenadores.add(entrenador);
}

//Añadir un nuevo pokemon a la lista pokemons
public void agregarPokemon(Pokemon pokemon){
    pokemons.add(pokemon);
} 

//Metodo que retorna una lista de entrenadores
public List<Entrenador> obtenerListaEntrenador(){
    return entrenadores;    
}

//Método que retorna una lista de pokemons
public List<Pokemon> obtenerListaPokemon(){
    return pokemons;
}

//Metodo para verificar si el pokemon existe ingresando el nombre:
public boolean verificarPokemon(String nombre){
    for (Pokemon p: pokemons){
        if (p.getNombre().equals(nombre)){
            return true;
        }
    }
    return false;
}
}

