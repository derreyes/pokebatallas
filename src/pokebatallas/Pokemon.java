/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokebatallas;

/**
 *
 * @author Dario Erreyes
 */
public class Pokemon {
    private String nombre, especie, vivo;
    private int ataque, defensa, nivel;

    // Constructor por defecto
    public Pokemon() {
    }
    
    // Constructor copia
    public Pokemon(Pokemon pokemon){
        this.nombre=pokemon.nombre;
        this.especie=pokemon.especie;
        this.vivo=pokemon.vivo;
        this.ataque=pokemon.ataque;
        this.defensa=pokemon.defensa;
        this.nivel=pokemon.nivel;
    }            
    
    //Constructor por parametros
    public Pokemon(String nombre, String especie, int ataque, int defensa, int nivel) {
        this.nombre = nombre;
        this.especie = especie;
        this.vivo = vivo;
        this.ataque = ataque;
        this.defensa = defensa;
        this.nivel = nivel;
    }

    //obteniendo los getters y setters, dado que los atributos son privados 
    public String getNombre() {
        return nombre;
    }

    public String getEspecie() {
        return especie;
    }

    public String getVivo() {
        return vivo;
    }

    public int getAtaque() {
        return ataque;
    }

    public int getDefensa() {
        return defensa;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public void setVivo(String vivo) {
        this.vivo = vivo;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    
    }
